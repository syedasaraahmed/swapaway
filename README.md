Swap Away Challenge
===========================

SRC LINK:
https://bitbucket.org/syedasaraahmed/swapaway/src/master/
CHALLENGE:
As I begin to work on the task, I noticed there were broken links and I couldn't find any docs, even in the link that were provided later. 
The link is “http” which has been blocked by google due to security reason so it must be migrated to “https” in order to be able to access it.
Also, I am a unity/C-# game developer so developing on Java-script, that too on GC devkit was new to me, as required by the task. 
IMPLEMENTATION:
DAY 1:
On my first day of the test, I spent some time on R&D to get to know Game Closure SDK. There were absolutely no tutorials. This was the biggest challenge for me to get started beyond "Hello world". 
DAY 2:
I finally found a very old template and went through the code to understand the structure. I was lucky enough to find an old fork of the documentation to understand basics like audio, and UI components. I designed a rough main menu screen.
DAY 3:
I decided to go with the “gem-swapper” game implementation, as I myself would like to implement it in unity (and play on my way back home in the train ;) ). The approach is different but the algorithm is similar. I needed to make sure I understand the magic behind what looks so simple as a casual game. To my surprise it was a little complicated. The following were my main focus:
LEVEL CREATION ALGORITHM:
o    To make sure that the gem to the left and the gem to the top are not of same color if the gem to the right or the gem to the bottom are of the same color as the current one.
o    To make sure that there is at least one possible match after a swap.
GAME PLAY ALGORITHM:
o    To make sure that the gem to the left and the gem to the top are of same color if the gem to the right and/or the gem to the bottom are of the same color as the current one.

DAY 4 - 5:
By this day, I learnt how the screen transition and audio works, however I was still figuring out to put SFX during game play. I learnt about emitters and it solved the issue. The next thing I did was to make sure the gem swap animation is shown before the matched gems are destroyed. Later I added the win / lose logic by adding goal score to achieve.
Things done:
o    Main Menu 
o    Background music and SFX
o    Swap Animation
o    Goal -> Win/lose logic 
o    Score screen
DAY 6:
I was a little overwhelmed and frustrated by the fact that there is absolutely no examples or tutorial or documentation for the particle effects. I wanted to beautify the game. It was way too time consuming, so I stopped it right there.
RECOMMENDATION:
If we are not affiliated with Game closure, may be it’s time we look in to other robust java-script game frameworks/SDKs.
Compared to Cocos-2dx (also known as Cocos-creator) and Unity 3d (as I have worked on both), I find GC a little outdated and not maintained. I lost my interest very easily.
Even if we want to continue working on it, we have to make sure it is updated it timely manner and more support is provided. More tutorials should be there and more examples. 
