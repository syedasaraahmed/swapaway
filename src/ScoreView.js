import device;
import AudioManager;
import ui.View;
import ui.TextView;
import ui.ImageView;
import ui.resource.Image as Image;

exports = Class(ui.View, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);
		this.addTitle();
		this.addScore();
		this.addRestartBtn();
	};

	this.addTitle = function() {
        var bg_image = new Image({ url: "resources/images/ui/background1.jpg" });

        this._bg = new ui.ImageView({
            superview: this,
            image: bg_image,
            x: 0,
            y: 0,
            width: 700,
            height: 1024,
            visible: true
        });

        this.statusView = new ui.TextView({
            superview: this,
            width: 600,
            height: 200,
            x: 50,
            y: 0,
            text: "Game Over!",
            fontFamily: "NikolaidisHand",
            color: '#ff951d'
        });

        this.titleView = new ui.TextView({
            superview: this,
            x: 50,
            y: 160,
            width: 600,
            height: 200,
            text: "Score",
            fontFamily: "Arcade",
            color: '#ffffff',
            autoSize: false
        });
	};

	this.addScore = function() {
		this.messageView = new ui.TextView({
			superview: this,
			x: 50,
			y: 300,
			width: 600,
			height: 480,
			size: 100,
			color: "#b01bff",
            fontFamily: 'Arcade',
			wrap: true
		});
	};

	this.addRestartBtn = function() {
        this.promptView = new ui.TextView({
            superview: this,
            x: 50,
            y: 650,
            width: 600,
            height: 480,
            size: 160,
            fontFamily: 'mrsunshine',
            text: "Play Again",
            color: "#ff0099"
        });
		this.restartBtn = new ui.View({
			superview: this,
			x: 0,
			y: 0,
			width: 700,
			height: 1024
		});
		this.restartBtn.on('InputSelect', bind(this, function() {
			this.emit('score:restart');
		}));
	};
});