import ui.ParticleEngine as ParticleEngine;
import src.Random as Random;
import ui.resource.Image as Image;

exports = Class(ParticleEngine, function(supr) {
    /**
     * Init
     */
    this.init = function(opts) {
        opts = merge(opts, {
            superview: superview,
            width: 43,
            height: 43,
            initCount: 2000,
            zIndex: 7,
            centerAnchor: true
        });
        supr(this, 'init', [opts]);
        this.setup();
    };

    this.setup = function () {
        var particles = this;

        this.on('particles:ball:destroy', function (gem) {
            console.log("gemDestroy");
            if (!gem) return;
            var particles = this.make();
            for (var i = 0; i < particles.length; i++) {
                var pObj = particles[i];
                pObj.polar = true;

                pObj.ox = gem.x + gem.width / 2 + Random.integer(-15, 15);
                pObj.oy = gem.y + gem.height / 2 + Random.integer(-15, 15);

                pObj.dy = Random.integer(-20, 20);
                pObj.dx = Random.integer(-20, 20);

                pObj.ttl = 4000;
                pObj.x = gem.x + gem.width / 2;
                pObj.y = gem.y + gem.height / 2;

                pObj.x += Random.integer(-20, 20);
                pObj.y += Random.integer(-20, 20);

                pObj.dopacity = Random.float(-0.4, -0.8);
                pObj.transition = "easeOut";
            }
            this.emitParticles(particles);
        });
    };
    /**
     * Get particles
     */
    this.make = function() {
        var count = 15;
        var particleObjects = this.obtainParticleArray(count);
        for (var i = 0; i < count; i++) {
            var pObj = particleObjects[i];
            pObj.image = "resources/images/particles/gold/mid_3.png";
            pObj.width = 15;
            pObj.height = 15;
            pObj.ttl = 4000;
        }
        return particleObjects;
    };

    /**
     * Particle engine tick
     */
    this.update = function(dt) {
        this.runTick(dt * 3);
    }

});
