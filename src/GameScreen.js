import device;
import animate;
import ui.View;
import ui.ImageView;
import ui.TextView;
import ui.resource.Image as Image;
import src.Gem as Gem;
import src.Particles as Particles;
import src.Config as Config;
import src.Transition as Transition;
// initial values
var x_offset = 8,
	y_offset = 150,
	gem_margin = 5,
	board_size = 7,
	game_length = 25000;

exports = Class(ui.View, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {
			x: 0,
			y: 0,
			width: 320,
			height: 480,
			backgroundColor: '#111'
		});
		supr(this, 'init', [opts]);
		this.build();
	};

	this.build = function () {
		var game = this;

		this.on('game:start', function() {
			startGame.call(game);
		});

		this.buildSubviews();
		this._gems = this.buildGemMatrix();

		this.interval = 100;
		this.total_elapsed_times = [];

		this.high_score = 0;
		this.scores = [];
		this.goal_score = 1000;
		this._goalscore.setText("Goal: " + this.goal_score);
		this.points_views = [];

		this.selected_gem = null;
		this.input_disabled = false;

		bindGemEvents.call(this);
	};

	this.buildSubviews = function() {
		this._high_score = new ui.TextView({
			superview: this,
			x: -22,
			y: 22,
			width: 341,
			height: 100,
			size: 65,
			horizontalAlign: "center",
			wrap: false,
			color: '#FFFFEE'
		});
        var bg_image = new Image({ url: "resources/images/ui/background4.jpg" });
        this._bg = new ui.ImageView({
            superview: this,
            image: bg_image,
            x: 0,
            y: 0,
            width: 700,
            height: 1000,
            visible: true
        });
        var clock = new Image({ url: "resources/images/ui/clock.png" });
        this._clockimg = new ui.ImageView({
            superview: this,
            image: clock,
            x: 25,
            y: 20,
            width: 100,
            height: 100,
            visible: true
        });
        this._clock = new ui.TextView({
            superview: this,
            x: 150,
            y: 20,
            width: 341,
            height: 100,
            size: 80,
            horizontalAlign: "left",
            wrap: false,
            color: '#000'
        });
        var score = new Image({ url: "resources/images/ui/trophy.png" });
        this._scoreimg = new ui.ImageView({
            superview: this,
            image: score,
            x: 570,
            y: 20,
            width: 100,
            height: 100,
            visible: true
        });

        this._score = new ui.TextView({
            superview: this,
            x: 220,
            y: 20,
            width: 341,
            height: 100,
            size: 80,
            horizontalAlign: "right",
            wrap: false,
            color: '#000'
        });

        this._goalscore = new ui.TextView({
            superview: this,
            x: 180,
            y: 850,
            width: 500,
            height: 200,
			size: 200,
            horizontalAlign: "center",
            wrap: false,
            color: '#ff0315',
			fontFamily: "mrsunshine",
			text: "Goal: "
        });
	};

	this.buildGemMatrix = function() {
		var gems = [];

		for (var row = 0; row < board_size; row++) {
			gems.push([]);

			for (var col = 0; col < board_size; col++) {
				//method to prevent streaks longer than 2 being generated on initialization

				var forbidden_types = [];

				if (col - 2 > -1) {
					if (gems[row][col - 1].gemtype === gems[row][col - 2].gemtype) {
						forbidden_types.push(gems[row][col - 1].gemtype);
					}
				}

				if (row - 2 > -1) {
					if (gems[row - 1][col].gemtype === gems[row - 2][col].gemtype) {
						forbidden_types.push(gems[row - 1][col].gemtype);
					}
				}

				var gem = new Gem({ row: row, col: col, forbidden_types: forbidden_types });

				gem.style.y = y_offset + row * (gem.style.height + gem_margin);
				gem.style.x = x_offset + col * (gem.style.width + gem_margin);

				this.addSubview(gem);

				gems[row].push(gem);
			}
		}

		return gems;
	};
});

function startGame() {
	var game = this;

	this.score = 0;
	this._high_score.setText(this.high_score);
	this.score_multiplier = 1;
	this.cleared_gems = 0;

	this.game_length = 25000;
	this.game_time = this.game_length;
	this.total_elapsed_time = 0;

	this.game_timer = setInterval(function() {
		tick.call(game);
	}, this.interval);
}

function tick() {
	if (this.game_time > 0) {
		var time_ratio = this.game_time / this.game_length;

		if (time_ratio < .33) {
			this._clock.updateOpts({ color: 'red' });
			this._clock.updateOpts({ size: 80 });
		} else {
			this._clock.updateOpts({ color: 'black' });
			this._clock.updateOpts({ size: 65 });
		}

		if (this.game_time > this.game_length) { this.game_time = this.game_length; }
		this._clock.setText(Math.round(this.game_time / 1000));
		this.game_time -= 100;

		this.total_elapsed_time += this.interval;
		this._score.setText(this.score);
	} else {
		endGame.call(this);
	}
}

function endGame() {
	clearInterval(this.game_timer);
	this.game_time = this.game_length;
	this.emit('game:end');
}

function bindGemEvents() {
	var that = this;

	this._gems.forEach(function(row) {
		row.forEach(function(gem) {
			gem.on('gem:clicked', function() {
				handleGemClick.call(that, gem);
			});
		});
	});
}

function bindSomeGemEvents(gems_to_bind) {
	var that = this;

	gems_to_bind.forEach(function(gem) {
		gem.on('gem:clicked', function() {
			handleGemClick.call(that, gem);
		});
	});
}

function handleGemClick(gem) {
	var game = this;

	if (this.input_disabled === false) {
		if (this.selected_gem !== null) {
			diff_y = gem.row - this.selected_gem.row,
			diff_x = gem.col - this.selected_gem.col;

			if (gem === this.selected_gem) {
				deselectGem.call(this);
			} else if (Math.abs(diff_x) > 1 || Math.abs(diff_y) > 1 || (Math.abs(diff_x) === 1 && Math.abs(diff_y) === 1)) {
				selectGem.call(this, gem);
			} else {
				attemptMove.call(this, gem);
			}

		} else {
			selectGem.call(this, gem);
		}
	}
}

function deselectGem() {
	this.selected_gem = null;
}

function selectGem(gem) {
	this.selected_gem = gem;
	this.emit('game:gemselect');
}

function attemptMove(gem) {
	var game = this;

    this.emit('game:gemselect');
	var mov_y = gem.row - this.selected_gem.row,
		mov_x = gem.col - this.selected_gem.col,
		move = [mov_y, mov_x];

	var valid_move = validMove.call(this, this.selected_gem, move);
	if (valid_move === false) { valid_move = validMove.call(this, gem, [-mov_y, -mov_x]); }

	if (valid_move === true) {
		var game = this;
		var selected_gem = this.selected_gem;
		var affected_gem = gem;
        game._gems[selected_gem.row][selected_gem.col] = affected_gem;
        game._gems[affected_gem.row][affected_gem.col] = selected_gem;
        var temp_row = selected_gem.row;
        var temp_col = selected_gem.col;
        var temp_y = selected_gem.style.y;
        var temp_x = selected_gem.style.x;
        var temp2_row = affected_gem.row;
        var temp2_col = affected_gem.col;
        var temp2_y = affected_gem.style.y;
        var temp2_x = affected_gem.style.x;

		var animation = new Transition(
			300,
			function(p){ return Math.pow(p, 3)},
			function (step) {
                selected_gem.style.y = temp_y - (temp_y - temp2_y) * step ;
                selected_gem.style.x = temp_x - (temp_x - temp2_x) * step  ;
                affected_gem.style.y = temp2_y - (temp2_y - temp_y) * step ;
                affected_gem.style.x = temp2_x - (temp2_x - temp_x) * step  ;
			},
			function () {
                selected_gem.row = temp2_row;
                selected_gem.col = temp2_col;
                affected_gem.row = temp_row;
                affected_gem.col = temp_col;
                var gems_to_destroy = findGemStreaks.call(game);
                clearGems.call(game, gems_to_destroy);
                game.selected_gem = null;
                game.emit('game:gempop');
			}
		);
		this.input_disabled = true;
		animation.start();

	} else {
		this.selected_gem = null;
	}
}

function findGemStreaks() {
	var gems = this._gems;
	var gems_to_destroy = [];
	var row_index = 0;

	//search through rows, grabbing horizontal streaks 3 or longer
	gems.forEach(function(row) {
		var col_index = 0;
		var consecutive = 0;

		row.forEach(function(gem) {
			if (col_index > 0) {
				if (gem.gemtype === gems[row_index][col_index - 1].gemtype) {
					consecutive++;

					if (consecutive === 2) {
						gems_to_destroy.push(gems[row_index][col_index - 1]);
						gems_to_destroy.push(gems[row_index][col_index - 2]);
						gems_to_destroy.push(gem);
					} else if (consecutive > 2) {
						gems_to_destroy.push(gem);
					}
				} else {
					consecutive = 0;
				}
			}
			col_index++;
		});

		row_index++;
	});

	//search through columns, grabbing vertical streaks 3 or longer
	//(and only including gems not already added above)
	for (var col = 0; col < board_size; col++) {
		var consecutive = 0;

		for (var row = 0; row < board_size; row++) {
			var gem = gems[row][col];

			if (row > 0) {
				if (gem.gemtype === gems[row - 1][col].gemtype) {
					consecutive++;

					if (consecutive === 2) {
						if (gems_to_destroy.indexOf(gems[row - 1][col]) === -1) {
							gems_to_destroy.push(gems[row - 1][col]);
						}
						if (gems_to_destroy.indexOf(gems[row - 2][col]) === -1) {
							gems_to_destroy.push(gems[row - 2][col]);
						}
						if (gems_to_destroy.indexOf(gem) === -1) {
							gems_to_destroy.push(gem);
						}
					} else if (consecutive > 2) {
						if (gems_to_destroy.indexOf(gem) === -1) {
							gems_to_destroy.push(gem);
						}
					}
				} else {
					consecutive = 0;
				}
			}
		}
	}

	return gems_to_destroy;
}

function clearGems(gems_to_destroy) {
	var game = this;

	this.cleared_gems += gems_to_destroy.length;

	if (gems_to_destroy.length === 3) {
		this.score_multiplier = 1;
	} else if (gems_to_destroy.length > 3) {
		this.score_multiplier += gems_to_destroy.length - 3;
	}

	var points = this.score_multiplier * gems_to_destroy.length * 10;
	this.score += points;
	this.game_time += Math.sqrt(this.score_multiplier) * gems_to_destroy.length * 250;

	//make game get progressively more difficult the longer it lasts
	if (this.game_length > 10000) {
		this.game_length = game_length - this.total_elapsed_time / 10;
	}

	if (points !== 0) {
		var points_view = new ui.TextView({
			superview: this,
			x: parseInt(-150 + 300 * Math.random()),
			y: 900,
			width: 576,
			height: 150,
			horizontalAlign: "center",
			size: points,
			text: "+" + points,
			color: "yellow"
		});

		this.addSubview(points_view);

		var points_animation = setInterval(function() {
			points_view.style.y -= 4.5;
		}, 50);

		setTimeout(function() {
			game.removeSubview(points_view);
			clearInterval(points_animation);
		}, 1000);
	}

	if (gems_to_destroy.length > 0) {
		var game = this;
		var num_timers_finished = 0;

		gems_to_destroy.forEach(function(gem) {
			var ticker = 0;

            // console.log("Show particles here");
            // var particles = new Particles();
            // particles.emit('particles:ball:destroy', gem);

			// destroy gem
			var shrink_animation = setInterval(function() {
				ticker++;
				if (ticker < 10) {
					gem.gem.style.width -= 20;
					gem.gem.style.height -= 20;
					gem.gem.style.x += 10;
					gem.gem.style.y += 10;
				} else {
					game._gems[gem.row][gem.col] = null;
					game.removeSubview(gem);
					num_timers_finished++;
					clearInterval(shrink_animation);
				}
			}, 20)
		});

		var animation_wait_timer = setInterval(function() {
			if (num_timers_finished === gems_to_destroy.length) {
				dropExisting.call(game);
				clearInterval(animation_wait_timer);
			}
		}, 30);
	} else {
		//if we are done destroying gems, re-enable inputs
		this.input_disabled = false;
	}
}

function dropExisting() {
	var game = this;
	var gems = this._gems;

	//starting with the second-to-bottom row, go through the columns looking for gems with holes in
	//the row beneath (destroyed gems). if one is found, move it down until another gem or the bottom
	//row is encountered.

	for (var row = board_size - 2; row > -1; row--) {
		for (var col = 0; col < board_size; col++) {
			if (gems[row][col] !== null) {
				var gem = gems[row][col];

				var step_down = 1;

				while (row + step_down < board_size && gems[row + step_down][col] === null) {
					performSingleMove.call(game, gem, [row + step_down, col]);

					step_down++;
				}
			}
		}
	}

	dropNew.call(this);
}

function dropNew() {
	var game = this;
	var gems = this._gems;
	var new_gems = [];

	//starting with the first column, go up from the bottom to the top row looking for an empty space
	//(a hole not covered by existing gems) if one is found, we know that all the rows above it in that
	//column will also be empty. proceed to fill those empty spaces with new gems.

	for (var col = 0; col < board_size; col++) {
		var num_gems;

		for (var row = board_size - 1; row > -1; row--) {
			if (gems[row][col] === null) {
				num_gems = row + 1;

				for (var i = row; i > -1; i--) {
					var gem = new Gem({ row: i, col: col, forbidden_types: [] });

					gem.style.y = y_offset + i * (gem.style.height + gem_margin);
					gem.style.x = x_offset + col * (gem.style.width + gem_margin);

					gems[i][col] = gem;
					new_gems.push(gem);

					game.addSubview(gem);

                    this.emit('game:gemfall');
					gem.fallIn(y_offset + i * (gem.style.height + gem_margin));
				}
			}
		}
	}

	bindSomeGemEvents.call(game, new_gems);

	//pause for a second before attempting to clear any streaks created by the new gem placements.
	//this will loop until there are no remaining streaks.
	setTimeout(function() {
		clearGems.call(game, findGemStreaks.call(game));
	}, 250);
}

function performSingleMove(gem, destination) {
	this._gems[gem.row][gem.col] = null;
	this._gems[destination[0]][destination[1]] = gem;
	gem.row = destination[0];
	gem.col = destination[1];

	var diff_y = y_offset + gem.row * (gem.style.height + gem_margin) - gem.style.y;

	gem.style.y = y_offset + gem.row * (gem.style.height + gem_margin);
	gem.style.x = x_offset + gem.col * (gem.style.width + gem_margin);

    this.emit('game:gemfall');
	gem.fallIn(diff_y);
}

function validMove(gem, move) {
	//check that a move would create at least one triplet of alike gems.
	var gs = this._gems;
	var s_row = gem.row; //starting row of gem after move
	var s_col = gem.col; //starting col of gem after move
	var gtype = gem.gemtype;

	if (move[1] === 0) {
		//vertical move
		if (s_row + 3 * move[0] < board_size && s_row + 3 * move[0] > -1) {
			if (gtype === gs[s_row + 2 * move[0]][s_col].gemtype && gtype === gs[s_row + 3 * move[0]][s_col].gemtype) {
				return true;
			}
		}
		if (s_col + 2 < board_size) {
			if (gtype === gs[s_row + move[0]][s_col + 1].gemtype && gtype === gs[s_row + move[0]][s_col + 2].gemtype) {
				return true;
			}
		}
		if (s_col - 2 > -1) {
			if (gtype === gs[s_row + move[0]][s_col - 1].gemtype && gtype === gs[s_row + move[0]][s_col - 2].gemtype) {
				return true;
			}
		}
		if (s_col + 1 < board_size && s_col - 1 > -1) {
			if (gtype === gs[s_row + move[0]][s_col - 1].gemtype && gtype === gs[s_row + move[0]][s_col + 1].gemtype) {
				return true;
			}
		}
		return false;
	} else {
		//horizontal move
		if (s_col + 3 * move[1] < board_size && s_col + 3 * move[1] > -1) {
			if (gtype === gs[s_row][s_col + 2 * move[1]].gemtype && gtype === gs[s_row][s_col + 3 * move[1]].gemtype) {
				return true;
			}
		}
		if (s_row + 2 < board_size) {
			if (gtype === gs[s_row + 1][s_col + move[1]].gemtype && gtype === gs[s_row + 2][s_col + move[1]].gemtype) {
				return true;
			}
		}
		if (s_row - 2 > -1) {
			if (gtype === gs[s_row - 1][s_col + move[1]].gemtype && gtype === gs[s_row - 2][s_col + move[1]].gemtype) {
				return true;
			}
		}
		if (s_row + 1 < board_size && s_row - 1 > -1) {
			if (gtype === gs[s_row - 1][s_col + move[1]].gemtype && gtype === gs[s_row + 1][s_col + move[1]].gemtype) {
				return true;
			}
		}
		return false;
	}
}