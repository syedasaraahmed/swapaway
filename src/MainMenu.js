import ui.View;
import ui.TextView;
import ui.ImageView;
import ui.resource.Image as Image;

exports = Class(ui.View, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);
        this.addImages();
		this.addTitle();
		this.addMessage();
		this.addStartButton();
	};

    this.addImages = function() {
        var bg_image = new Image({ url: "resources/images/ui/background1.jpg" });

        this._bg = new ui.ImageView({
            superview: this,
            image: bg_image,
            x: 0,
            y: 0,
            width: 700,
            height: 1024,
            visible: true
        });
        var gem1 = new ui.ImageView({
            superview: this,
            image: 'resources/images/gems/blue.png',
            width: 68,
            height: 68,
            x: 60,
            y: 750
        });
        var gem2 = new ui.ImageView({
            superview: this,
            image: 'resources/images/gems/pink.png',
            width: 68,
            height: 68,
            x: 160,
            y: 750
        });
        var gem3 = new ui.ImageView({
            superview: this,
            image: 'resources/images/gems/yellow.png',
            width: 68,
            height: 68,
            x: 260,
            y: 750
        });
        var gem4 = new ui.ImageView({
            superview: this,
            image: 'resources/images/gems/green.png',
            width: 68,
            height: 68,
            x: 360,
            y: 750
        });
        var gem5 = new ui.ImageView({
            superview: this,
            image: 'resources/images/gems/orange.png',
            width: 68,
            height: 68,
            x: 460,
            y: 750
        });
        var gem6 = new ui.ImageView({
            superview: this,
            image: 'resources/images/gems/purple.png',
            width: 68,
            height: 68,
            x: 560,
            y: 750
        });
    };

	this.addTitle = function() {
        this.titleImageview = new ui.ImageView({
            superview: this,
            image: 'resources/images/ui/title.png',
            width: 600,
            height: 100,
            x: 50,
            y: 50
        });

        this.titleView = new ui.TextView({
            superview: this,
            x: 50,
            y: -280,
            width: 600,
            height: 100,
            text: "Challenge",
            fontFamily: "Arcade",
            color: '#fff',
            autoSize: false
        });
	};

	this.addMessage = function() {
		this.messageView = new ui.TextView({
			superview: this,
			x: 43,
			y: 85,
			width: 320,
			height: 480,
			size: 30,
			color: "white",
			wrap: true
		});
	};

	this.addStartButton = function() {
        this.promptView = new ui.TextView({
            superview: this,
            x: 50,
            y: 30,
            width: 320,
            height: 480,
            size: 160,
            fontFamily: 'mrsunshine',
            text: "START",
            color: "#ff0099"
        });
		this.startbutton = new ui.View({
			superview: this,
			x: 0,
			y: 0,
			width: 320,
			height: 480
		});

		this.startbutton.on('InputSelect', bind(this, function() {
			this.emit('menu:start');
		}));
	};
});