import device;
import ui.StackView as StackView;
import AudioManager;

import src.MainMenu as MainMenu;
import src.GameScreen as GameScreen;
import src.ScoreView as ScoreView;
import src.util as util;
import AudioManager;

var rootView, app;
var titlescreen = new MainMenu(),
    scoreView = new ScoreView(),
    gamescreen = new GameScreen();

exports = Class(GC.Application, function () {
	this.initUI = function () {
		util.scaleRootView(this, 683, 1024);
		this.view.style.backgroundColor = '#000';
        this.setupAudio();
		this.setupScreens();
	};
	this.setupScreens = function() {
        rootView = new StackView({
            superview: this.view,
            x: 0,
            y: 0,
            width: 683,
            height: 1024,
            clip: true,
            backgroundColor: '#000'
        });

        app = this;
        titlescreen.width = 576;
        titlescreen.height = 1024;

        titlescreen.titleView.style.width = 576;
        titlescreen.titleView.style.x = 50;
        titlescreen.titleView.style.height = 1024;

        titlescreen.promptView.style.width = 576;
        titlescreen.promptView.style.height = 1024;

        titlescreen.messageView.style.width = 576;
        titlescreen.messageView.style.height = 1024;

        titlescreen.startbutton.style.width = 576;
        titlescreen.startbutton.style.height = 1024;

        rootView.push(titlescreen);

        titlescreen.on('menu:start', function() {
            app.emit('playaudio', 'level');
            rootView.push(gamescreen);
            gamescreen.emit('game:start');
        });

        // Game Screen
        gamescreen.on('game:end', function() {
            app.audio.stop('level');
            gamescreen.scores.push(gamescreen.score);
            gamescreen.total_elapsed_times.push(gamescreen.total_elapsed_time);
            var score_sum = 0;
            gamescreen.scores.forEach(function(score) {
                score_sum += score;
            });
            var time_sum = 0;
            gamescreen.total_elapsed_times.forEach(function(time) {
                time_sum += time / 1000;
            });
            if (gamescreen.score >= gamescreen.goal_score) {
                app.emit('playaudio', 'win');
                scoreView.statusView.setText("You Won!");
                if (gamescreen.score > gamescreen.high_score) {
                    scoreView.titleView.setText("Highscore: " + gamescreen.score);
                    gamescreen.high_score = gamescreen.score;
                } else {
                    scoreView.titleView.setText("Score: " + gamescreen.score);
                }
            } else {
                scoreView.statusView.setText("You Lost!");
                scoreView.titleView.setText("Score: " + gamescreen.score);
                app.emit('playaudio', 'fail');
            }
            scoreView.messageView.setText(
                "Gems Cleared\n" + gamescreen.cleared_gems
            );
            rootView.push(scoreView);
        });

        gamescreen.on('game:gemselect', function () {
            app.emit('playaudio', 'select');
        });
        gamescreen.on('game:gempop', function () {
            app.emit('playaudio', 'score');
        });
        gamescreen.on('game:gemfall', function () {
            app.emit('playaudio','fall');
        });
        scoreView.on('score:restart', function () {
            app.emit('playaudio', 'level');
            gamescreen.emit('game:start');
            rootView.pop();
        })
	};
    this.setupAudio = function () {
        this.audio = new AudioManager({
            path: 'resources/sounds',
            files: {
                level: {
                    path: 'music',
                    volume: 0.2,
                    background: true
                },
                win: {
                    path: 'effect'
                },
                fail: {
                    path: 'effect'
                },
                select: {
                    path: 'effect'
                },
                score: {
                    path: 'effect',
                    volume: 0.3
                },
                fall: {
                    path: 'effect'
                }
            }
        });
        this.on('playaudio', function (audioName) {
            this.audio.play(audioName);
        });
    };
	this.launchUI = function () {};
});
