exports = {
    particles: {
        sizes: {
            big: [40, 40],
            mid: [15, 15],
            small: [7, 7]
        },
        path: 'resources/images/particles/',
        items: {
            gold_big: ["big.png"],
            gold_mid: ["mid.png", "mid_2.png", "mid_3.png", "mid_4.png", "mid_5.png"],
            gold_small: ["small.png", "small_2.png", "small_3.png", "small_4.png"]
        }
    }
};
